db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$count: "itemsRedFarm"}
])



db.fruits.aggregate([
    {$match: {price:{$gt: 50}}},
    {$count: "itemsGreaterThan50"}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id: "$supplier", avgPricePerSupplier: {$avg: "$price"}}}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id: "$supplier", maxPricePerSupplier: {$max: "$price"}}}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id: "$supplier", minPricePerSupplier: {$min: "$price"}}}
])

//Additional activity
db.fruits.aggregate([
    {$match: {supplier: "Yellow Farms"}},
    {$group: {_id:"yellowFarmsAveragePrice", avgPrice: {$avg: "$price"}}}
])

db.fruits.aggregate([
    {$match: {stocks: {$lt: 20}}},
    {$group: {_id:"maxPriceStocksLessThan20", maxPrice: {$max: "$price"}}}
])

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"lowestPriceAllItems", minPrice: {$min: "$price"}}}
])

